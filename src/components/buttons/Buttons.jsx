import './Buttons.module.scss'


function Button (props){

const style = {
    background:props.backgroundcolor,
}  
    // width: "230px",
    // heigth: "100px",
    // padding: "25px",
    // margin:'10px',
    // border: 'none',
    // borderRadius: '10px',
    // boxSize: 'border-box',
    // fontSize: '20px',
    // cursor: 'pointer'


    return(
        <button 
        className='btn' 
        onClick={props.onClick} 
        style={style}
        >{props.text}</button>
        // className 
    )
}

export default Button