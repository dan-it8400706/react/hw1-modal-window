import style from "./Modal.module.scss";
import Button from "../buttons/Buttons";

function Modal(props) {

    return (
        <div className={style.div}>
            <h3 className={style.h3}>{props.header}
                <span onClick={props.actions}>X</span>
            </h3>
            <p className={style.p}>{props.text}</p>
            <Button 
                onClick={props.actions} 
                text={props.buttonFirstText}
            ></Button>
            <Button
                onClick={props.actions} 
                text={props.buttonSecondText}
                backgroundcolor = {props.backgroundcolor}
            ></Button>
        </div>
    )
}

export default Modal